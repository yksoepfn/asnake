#!/usr/bin/env python

import pixmaps

from playsound import playsound

SCREEN_WIDTH = 40
SCREEN_HEIGHT = 30

BLOCK_HEIGHT = 20
BLOCK_WIDTH = 20

import sys
import random

try:
    from PySide2.QtGui import QStandardItemModel
    from PySide2.QtGui import QStandardItem
    from PySide2.QtGui import QPixmap
    from PySide2.QtGui import QIcon
    from PySide2.QtGui import QPainter
    from PySide2.QtGui import QPalette
    from PySide2.QtGui import QTransform
    from PySide2.QtCore import QRect
    from PySide2.QtCore import QSize
    from PySide2.QtCore import QTimer

    from PySide2.QtWidgets import QApplication
    from PySide2.QtWidgets import QMainWindow
    from PySide2.QtWidgets import QWidget
    from PySide2.QtWidgets import QHBoxLayout
    from PySide2.QtWidgets import QSplitter
    from PySide2.QtWidgets import QLabel
    from PySide2.QtWidgets import QTreeView
    from PySide2.QtWidgets import QAction
    from PySide2.QtWidgets import QMenu
    from PySide2.QtWidgets import QAbstractItemView
    from PySide2.QtWidgets import QScrollArea
    from PySide2.QtWidgets import QMessageBox
    from PySide2.QtWidgets import QDialog
    from PySide2.QtWidgets import QFrame
    from PySide2.QtWidgets import QVBoxLayout
    from PySide2.QtWidgets import QGroupBox
    from PySide2.QtWidgets import QRadioButton
    from PySide2.QtWidgets import QCheckBox
    from PySide2.QtWidgets import QLineEdit
    from PySide2.QtWidgets import QDialogButtonBox
    from PySide2.QtWidgets import QPushButton
    from PySide2.QtWidgets import QSpacerItem
    from PySide2.QtWidgets import QSizePolicy
    from PySide2.QtWidgets import QGridLayout
    from PySide2.QtWidgets import QFileDialog
    from PySide2.QtWidgets import QDialog
    from PySide2.QtWidgets import QStyle
    from PySide2.QtWidgets import QInputDialog

    from PySide2 import QtSvg
    from PySide2.QtCore import Qt
    from PySide2.QtCore import QObject
except ImportError:
    from PyQt5.QtGui import QStandardItemModel
    from PyQt5.QtGui import QStandardItem
    from PyQt5.QtGui import QPixmap
    from PyQt5.QtGui import QIcon
    from PyQt5.QtGui import QPainter
    from PyQt5.QtGui import QPalette
    from PyQt5.QtGui import QTransform

    from PyQt5.QtWidgets import QApplication
    from PyQt5.QtWidgets import QMainWindow
    from PyQt5.QtWidgets import QWidget
    from PyQt5.QtWidgets import QHBoxLayout
    from PyQt5.QtWidgets import QSplitter
    from PyQt5.QtWidgets import QLabel
    from PyQt5.QtWidgets import QTreeView
    from PyQt5.QtWidgets import QAction
    from PyQt5.QtWidgets import QMenu
    from PyQt5.QtWidgets import QAbstractItemView
    from PyQt5.QtWidgets import QScrollArea
    from PyQt5.QtWidgets import QMessageBox
    from PyQt5.QtWidgets import QDialog
    from PyQt5.QtWidgets import QFrame
    from PyQt5.QtWidgets import QVBoxLayout
    from PyQt5.QtWidgets import QGroupBox
    from PyQt5.QtWidgets import QRadioButton
    from PyQt5.QtWidgets import QCheckBox
    from PyQt5.QtWidgets import QLineEdit
    from PyQt5.QtWidgets import QDialogButtonBox
    from PyQt5.QtWidgets import QPushButton
    from PyQt5.QtWidgets import QSpacerItem
    from PyQt5.QtWidgets import QSizePolicy
    from PyQt5.QtWidgets import QGridLayout
    from PyQt5.QtWidgets import QFileDialog
    from PyQt5.QtWidgets import QStyle
    from PyQt5.QtWidgets import QInputDialog

    from PyQt5 import QtSvg
    from PyQt5.QtCore import Qt
    from PyQt5.QtCore import QObject
    from PyQt5.QtCore import QRect
    from PyQt5.QtCore import QSize
    from PyQt5.QtCore import QTimer

import enum

class Direction(enum.Enum):
    TOP = 0
    BOTTOM = 1
    RIGHT = 2
    LEFT = 3


class CollideBoundary(Exception):
    pass


class CollideSelf(Exception):
    pass


class PoisonException(Exception):
    pass


class MetaObject():

    def __init__(self, x, y, c):
        self.x = x
        self.y = y
        self.c = c

class Apple(MetaObject):
    def __init__(self, x, y, c = pixmaps.GENERIC_APPLE):
        super().__init__(x, y, c)

    def __str__(self):
        return "A, x: %(x)d, y: %(y)d" % {'x': self.x, 'y': self.y}


class Box(MetaObject):
    def __init__(self, x, y, c = pixmaps.GENERIC_BOX):
        super().__init__(x, y, c)

    
class Poison(MetaObject):
    def __init__(self, x, y, c = pixmaps.GENERIC_POISON):
        super().__init__(x, y, c)


class Block(MetaObject):
    def __init__(self, x, y, c = pixmaps.GENERIC_BLOCK):
        super().__init__(x, y, c)

    def __eq__(self, other):
        if self.x == other.x and self.y == other.y:
            return True
        return False

    def follow(self, c):
        self.x = c.x
        self.y = c.y

    def left(self):
        if self.x == 0:
            raise CollideBoundary()
        else:
            self.x -= 1

    def right(self):
        if self.x >= SCREEN_WIDTH-1:
            raise CollideBoundary()
        else:
            self.x += 1

    def top(self):
        if self.y == 0:
            raise CollideBoundary()
        else:
            self.y -= 1

    def bottom(self):
        if self.y >= SCREEN_HEIGHT-1:
            raise CollideBoundary()
        else:
            self.y += 1


class Snake():
    def __init__(self, game, direction, blocks):
        self.blocks = []
        game.snake = self
        self.direction = direction
        if blocks:
            for block in blocks:
                x, y = block
                b = Block(x, y)
                self.blocks.append(b)
        self.head = self.blocks[0]
        self.__update_head()
        self.grow = False

    def __update_head(self):
        if self.direction == Direction.RIGHT:
            self.head.c = pixmaps.SNAKE_RIGHT
        elif self.direction == Direction.LEFT:
            self.head.c = pixmaps.SNAKE_LEFT
        elif self.direction == Direction.TOP:
            self.head.c = pixmaps.SNAKE_TOP
        elif self.direction == Direction.BOTTOM:
            self.head.c = pixmaps.SNAKE_BOTTOM

    def move(self):
        if self.direction == Direction.RIGHT:
            self.right()
        elif self.direction == Direction.LEFT:
            self.left()
        elif self.direction == Direction.TOP:
            self.top()
        elif self.direction == Direction.BOTTOM:
            self.bottom()

    def __follow_all(self):
        t = self.blocks[-1::-1]
        last = self.blocks[-1]
        bl = Block(last.x, last.y, last.c)
        for i in range(len(t)):
            try:
                b = t[i]
                c = t[i+1]
                b.follow(c)
            except IndexError:
                pass
        if self.grow:
            self.blocks.append(bl)
            self.grow = False

    def collide(self):
        for b in self.blocks[1:]:
            if self.head == b:
                raise CollideSelf()

    def left(self):
        if self.direction != Direction.RIGHT:
            self.__follow_all()
            self.head.left()
            self.direction = Direction.LEFT
            self.__update_head()
            self.collide()

    def right(self):
        if self.direction != Direction.LEFT:
            self.__follow_all()
            self.head.right()
            self.direction = Direction.RIGHT
            self.__update_head()
            self.collide()

    def top(self):
        if self.direction != Direction.BOTTOM:
            self.__follow_all()
            self.head.top()
            self.direction = Direction.TOP
            self.__update_head()
            self.collide()

    def bottom(self):
        if self.direction != Direction.TOP:
            self.__follow_all()
            self.head.bottom()
            self.direction = Direction.BOTTOM
            self.__update_head()
            self.collide()


class Screen(QLabel):

    def __init__(self, parent, width, height):
        super().__init__(parent)
        self.width = width
        self.height = height
        self.setGeometry(QRect(
            0,
            0,
            width * BLOCK_WIDTH,
            height * BLOCK_HEIGHT))
        self.game = None

    def sizeHint(self):
        return QSize(
            self.width * BLOCK_WIDTH,
            self.height * BLOCK_HEIGHT)

    def paint(self):
        c = QPixmap(pixmaps.GENERIC_GRASS)
        pi = QPixmap(
            SCREEN_WIDTH * BLOCK_WIDTH,
            SCREEN_HEIGHT * BLOCK_HEIGHT)
        pa = QPainter()
        pa.begin(pi)
        pa.drawTiledPixmap(
            0,
            0,
            self.width * BLOCK_WIDTH,
            self.height * BLOCK_HEIGHT,
            c)
        if self.game:
            for poison in self.game.poisons:
                qp = QPixmap(poison.c)
                pa.drawPixmap(
                    poison.x * BLOCK_WIDTH,
                    poison.y * BLOCK_HEIGHT,
                    BLOCK_WIDTH,
                    BLOCK_HEIGHT,
                    qp)
            for apple in self.game.apples:
                qp = QPixmap(apple.c)
                pa.drawPixmap(
                    apple.x * BLOCK_WIDTH,
                    apple.y * BLOCK_HEIGHT,
                    BLOCK_WIDTH,
                    BLOCK_HEIGHT,
                    qp)
            for box in self.game.boxes:
                qp = QPixmap(box.c)
                pa.drawPixmap(
                    box.x * BLOCK_WIDTH,
                    box.y * BLOCK_HEIGHT,
                    BLOCK_WIDTH,
                    BLOCK_HEIGHT,
                    qp)
            for block in self.game.snake.blocks:
                qp = QPixmap(block.c)
                pa.drawPixmap(
                    block.x * BLOCK_WIDTH,
                    block.y * BLOCK_HEIGHT,
                    BLOCK_WIDTH,
                    BLOCK_HEIGHT,
                    qp)
        pa.end()
        self.setPixmap(pi)


class Game():

    def __init__(self, screen, parent):
        screen.game = self
        self.apples = []
        self.poisons = []
        self.boxes = []
        self.score = 0
        self.parent = parent

        for i in range(0, 13):
            self.create_apple()
        for i in range(0, 9):
            self.create_poison()
        self.update_status()
        for  i in range(0, 11)
            self.create_box()

    def check_object(self, x, y):
        if len(self.apples) == 0 and len(self.boxes) == 0 and len(self.poisons) == 0:
            return True
        for apple in self.apples:
            if apple.x == x and apple.y == y:
                return False
        for poison in self.poisons:
            if poison.x == x and poison.y == y:
                return False
        for box in self.boxes:
            if box.x == x and box.y == y:
                return False
        return True

    def create_apple(self):
        ax = random.randint(0, SCREEN_WIDTH-1)
        ay = random.randint(0, SCREEN_HEIGHT-1)
        while not self.check_object(ax, ay):
            ax = random.randint(0, SCREEN_WIDTH-1)
            ay = random.randint(0, SCREEN_HEIGHT-1)         
        apple = Apple(ax, ay)
        self.apples.append(apple)
        return apple

    def create_box(self):
        ax = random.randrange(0, SCREEN_WIDTH-1)
        ay = random.randrange(0, SCREEN_HEIGHT-1)
        while not self.check_object(ax, ay):
            ax = random.randrange(0, SCREEN_WIDTH-1)
            ay = random.randrange(0, SCREEN_HEIGHT-1)
        box = Box(ax, ay)
        self.boxes.append(box)
        return box

    def create_poison(self):
        ax = random.randrange(0, SCREEN_WIDTH-1)
        ay = random.randrange(0, SCREEN_HEIGHT-1)
        while not self.check_object(ax, ay):
            ax = random.randrange(0, SCREEN_WIDTH-1)
            ay = random.randrange(0, SCREEN_HEIGHT-1)        
        poison = Poison(ax, ay)
        self.poisons.append(poison)
        return poison

    def update_status(self, msg = ""):
        status = 'Snake! | Your score: %(p)d' % {'p': self.score}
        if msg:
            status += ' | ' + msg
        self.parent.setWindowTitle(status)

    def rotate_left(self, snake):
        if snake.direction == Direction.LEFT:
            snake.direction = Direction.BOTTOM
        elif snake.direction == Direction.BOTTOM:
            snake.direction = Direction.RIGHT
        elif snake.direction == Direction.RIGHT:
            snake.direction = Direction.TOP
        elif snake.direction == Direction.TOP:
            snake.direction = Direction.LEFT

    def check_snake(self, snake):
        for apple in self.apples:
            if apple.x == snake.head.x and apple.y == snake.head.y:
#                self.score += 1
#                self.update_status()
                self.apples.remove(apple)
                self.play('1.wav')
#                new_apple = self.create_apple()
#                self.snake.grow = True
                return
        for box in self.boxes:
            if box.x == snake.head.x and box.y == snake.head.y:
                self.play('3.mp3')
                self.boxes.remove(box)
                self.rotate_left(snake)
                return
        for poison in self.poisons:
            if poison.x == snake.head.x and poison.y == snake.head.y:
                self.play('2.aiff')
                self.poisons.remove(poison)
                self.rotate_right(snake)
                return

    def play(file):
        thread = PlayThread(file)
        thread.start()


class HWindow(QMainWindow):

    def __init__(self):
        super().__init__()
        self.create_ui()

    def create_ui(self):
        self.screen = Screen(self, SCREEN_WIDTH, SCREEN_HEIGHT)
        self.game = Game(self.screen, self)
        self.snake = Snake(self.game,
                           Direction.RIGHT,
                           [(7, 5), (6, 5), (5, 5), (4, 5), (3, 5), (3, 4)])
        self.setCentralWidget(self.screen)
        self.screen.paint()
        self.timer = QTimer()
        self.timer.timeout.connect(self.tick)
        self.timer.start(200)

    def error(self, msg = ""):
            mb = QMessageBox()
            mb.setIcon(QMessageBox.Warning)
            mb.setText(msg)
            mb.setWindowTitle("O-O-O-P-S!")
            mb.setStandardButtons(QMessageBox.Ok)
            mb.exec_()

    def tick(self):
        try:
            self.snake.move()
            self.screen.paint()
            self.game.check_snake(self.snake)
        except CollideBoundary:
            self.timer.stop()
            self.error("You hit the boundary! You're dead!")
            sys.exit(0)
        except CollideSelf:
            self.timer.stop()
            self.error("You ate yourself! You're dead!")
            sys.exit(0)
        except PoisonException:
            self.timer.stop()
            self.error("You have been poisoned! You're dead!")
            sys.exit(0)

    def keyPressEvent(self, event):
        key = event.key()
        if key == Qt.Key_Left:
            self.snake.left()
            self.screen.paint()
            self.game.check_snake(self.snake)
        elif key == Qt.Key_Right:
            self.snake.right()
            self.screen.paint()
            self.game.check_snake(self.snake)
        elif key == Qt.Key_Up:
            self.snake.top()
            self.screen.paint()
            self.game.check_snake(self.snake)
        elif key == Qt.Key_Down:
            self.snake.bottom()
            self.screen.paint()
            self.game.check_snake(self.snake)

if __name__ == "__main__":
    random.seed()
    app = QApplication(sys.argv)
    window = HWindow()
    window.show()
    sys.exit(app.exec_())
