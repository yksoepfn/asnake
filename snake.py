#!/usr/bin/env python

import curses
import time
import random
import sys
import enum
import threading


class Direction(enum.Enum):
    UP = 0
    DOWN = 1
    RIGHT = 2
    LEFT = 3


class CollideBoundary(Exception):
    pass


class CollideSelf(Exception):
    pass


class PoisonException(Exception):
    pass

class MetaObject():
    def __init__(self, screen, y, x, c):
        self.y = y
        self.x = x
        self.c = c
        self.screen = screen

    def render(self):
        self.screen.board.addch(self.y, self.x, self.c)

    def clear(self):
        self.screen.board.addch(self.y, self.x, ' ')

    
class Apple(MetaObject):
    def __init__(self, screen, y, x, c='@'):
        MetaObject.__init__(self, screen, y, x, c)


class Poison(MetaObject):
    def __init__(self, screen, y, x, c='!'):
        MetaObject.__init__(self, screen, y, x, c)

    
class Block():
    def __init__(self, display, y, x, c='#'):
        self.y = y
        self.x = x
        self.c = c
        self.display = display

    def __eq__(self, other):
        return True if self.x == other.x and self.y == other.y else False

    def render(self):
        self.display.addch(self.y, self.x, self.c)

    def render_empty(self):
        self.display.addch(self.y, self.x, ' ')

    def left(self):
        if self.x == 0:
            raise CollideBoundary()
        else:
            self.x -= 1
            self.render()

    def right(self):
        y, x = self.display.getmaxyx()
        if self.x >= x-1:
            raise CollideBoundary()
        else:
            self.x += 1
            self.render()

    def top(self):
        if self.y == 0:
            raise CollideBoundary()
        else:
            self.y -= 1
            self.render()

    def bottom(self):
        y, x = self.display.getmaxyx()
        if self.y >= y-1:
            raise CollideBoundary()
        else:
            self.y += 1
            self.render()

    def follow(self, b):
        self.y = b.y
        self.x = b.x


class Snake():
    def __init__(
            self,
            screen,
            blocks = [(4,7), (3, 7), (3, 6), (3, 5), (3, 4), (3, 3)]):
        self.blocks = []
        self.screen = screen
        self.direction = Direction.RIGHT
        if blocks:
            for block in blocks:
                y, x = block
                b = Block(self.screen.board, y, x)
                self.blocks.append(b)
        self.head = self.blocks[0]

    def __follow_all(self):
        t = self.blocks[-1::-1]
        last = self.blocks[-1]
        last.render_empty()
        for i in range(len(t)):
            try:
                b = t[i]
                c = t[i+1]
                b.follow(c)
            except IndexError:
                pass
        self.render()

    def collide(self):
        for b in self.blocks[1:]:
            if self.head == b:
                raise CollideSelf()

    def left(self):
        if self.direction != Direction.RIGHT:
            self.__follow_all()
            self.head.c = '>'
            self.head.left()
            self.collide()
            self.direction = Direction.LEFT
        self.render()

    def right(self):
        if self.direction != Direction.LEFT:
            self.__follow_all()
            self.head.c = '<'
            self.head.right()
            self.collide()
            self.direction = Direction.RIGHT
        self.render()

    def top(self):
        if self.direction != Direction.DOWN:
            self.__follow_all()
            self.head.c = '^'
            self.head.top()
            self.collide()
            self.direction = Direction.UP
        self.render()

    def bottom(self):
        if self.direction != Direction.UP:
            self.__follow_all()
            self.head.c = 'v'
            self.head.bottom()
            self.collide()
            self.direction = Direction.DOWN
        self.render()

    def render(self):
        for b in self.blocks:
            b.render()
        self.screen.refresh()

class Screen():
    def __init__(self):
        self.stdscr = curses.initscr()
        curses.cbreak()
        curses.noecho()
        self.stdscr.keypad(1)
        curses.curs_set(0)
        curses.start_color()
        curses.init_pair(1, curses.COLOR_GREEN, curses.COLOR_BLACK)
        curses.init_pair(2, curses.COLOR_BLACK, curses.COLOR_WHITE)
        self.stdscr.bkgd(' ', curses.color_pair(2))
        height, width = self.stdscr.getmaxyx()
        self.status = curses.newwin(1, width, height-1, 0)
        self.board = curses.newwin(height-1, width, 0, 0)
        self.board.bkgd(' ', curses.color_pair(1))
        self.status.bkgd(' ', curses.color_pair(2))
        self.board.keypad(1)
        self.status.keypad(1)

    def refresh(self):
        self.board.refresh()
        self.status.refresh()

    def clear(self):
        self.status.clear()
        self.board.clear()

    def set_status(self, msg):
        self.status.addstr(0, 1, msg)
        self.status.refresh()

    def quit(self):
        curses.nocbreak()
        self.stdscr.keypad(0)
        self.board.keypad(0)
        self.status.keypad(0)
        curses.echo()
        curses.endwin()
        print("-- GAME OVER --")


class SnakeThread(threading.Thread):
    def __init__(self, snake, screen, game):
        threading.Thread.__init__(self)
        self.snake = snake
        self.enabled = True
        self.screen = screen
        self.game = game

    def stop(self):
        self.enabled = False

    def run(self):
        while self.enabled:
            try:
                if self.snake.direction == Direction.LEFT:
                    self.snake.left()
                    self.game.check_snake(self.snake)
                elif self.snake.direction == Direction.RIGHT:
                    self.snake.right()
                    self.game.check_snake(self.snake)
                elif self.snake.direction == Direction.UP:
                    self.snake.top()
                    self.game.check_snake(self.snake)
                elif self.snake.direction == Direction.DOWN:
                    self.snake.bottom()
                    self.game.check_snake(self.snake)

                time.sleep(0.1)
                self.snake.render()
                self.screen.refresh()
            except CollideBoundary:
                self.stop()
                self.game.update_status(
                    '--- OOPS! --- | --- GAME OVER --')
            except CollideSelf:
                self.stop()
                self.game.update_status(
                    '--- OOPS! --- | --- GAME OVER --')
            except PoisonException:
                self.stop()
                self.game.update_status(
                    '--- OOPS! --- | --- GAME OVER --')


class Game():
    def __init__(self, screen):
        self.screen = screen
        self.apples = []
        self.poisons = []
        self.score = 0
        random.seed()
        for i in range(0, 5):
            self.create_apple()
        for i in range(0, 2):
            self.create_poison()
        for apple in self.apples:
            apple.render()
        for poison in self.poisons:
            poison.render()
        self.update_status()

    def create_apple(self):
        y, x = self.screen.board.getmaxyx()
        ax = random.randint(0, x)
        ay = random.randint(0, y)
        apple = Apple(self.screen, ay, ax)
        self.apples.append(apple)
        return apple

    def create_poison(self):
        y, x = self.screen.board.getmaxyx()
        ax = random.randrange(0, x)
        ay = random.randrange(0, y)
        poison = Poison(self.screen, ay, ax)
        self.poisons.append(poison)
        return poison

    def update_status(self, msg = ""):
        status = 'Snake! | Your score: %(p)d' % {'p': self.score}
        if msg:
            status += ' | ' + msg
        self.screen.set_status(status)

    def check_snake(self, snake):
        for poison in self.poisons:
            if poison.x == snake.head.x and poison.y == snake.head.y:
                raise PoisonException()
        for apple in self.apples:
            for block in snake.blocks:
                if apple.x == block.x and apple.y == block.y:
                    self.score += 1
                    self.apples.remove(apple)
                    new_apple = self.create_apple()
                    new_apple.render()
                    self.screen.refresh()
                    self.update_status()


if __name__ == "__main__":
    screen = Screen()
    snake = Snake(screen)
    snake.render()
    game = Game(screen)
    st = SnakeThread(snake, screen, game)
    key = curses.KEY_RIGHT
    enabled = True
    st.start()

    try:
        while enabled:
            next_key = screen.board.getch()
            if next_key != -1:
                key = next_key
            if key == curses.KEY_RIGHT:
                snake.right()
            elif key == curses.KEY_LEFT:
                snake.left()
            elif key == curses.KEY_UP:
                snake.top()
            elif key == curses.KEY_DOWN:
                snake.bottom()
            elif key == 27:
                enabled = False
                st.stop()
                next_key = screen.board.getch()
                screen.clear()
                screen.quit()
                sys.exit(1)
    except KeyboardInterrupt:
        screen.clear()
        st.stop()
        screen.quit()
