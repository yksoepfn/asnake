#!/usr/bin/env python

import pixmaps

from playsound import playsound
from time import sleep

SCREEN_WIDTH = 60
SCREEN_HEIGHT = 45

BLOCK_HEIGHT = 20
BLOCK_WIDTH = 20

MAX_APPLES = 10
MAX_POISONS = 10
MAX_BOXES = 10
MAX_CUPS = 10
MAX_TREES = 12
MAX_FOXES = 10

import sys
import random

try:
    from PySide2.QtGui import QStandardItemModel
    from PySide2.QtGui import QStandardItem
    from PySide2.QtGui import QPixmap
    from PySide2.QtGui import QIcon
    from PySide2.QtGui import QPainter
    from PySide2.QtGui import QPalette
#    from PySide2.QtMultimedia import QSound
    from PySide2.QtCore import QRect
    from PySide2.QtCore import QSize
    from PySide2.QtCore import QTimer

    from PySide2.QtWidgets import QApplication
    from PySide2.QtWidgets import QMainWindow
    from PySide2.QtWidgets import QWidget
    from PySide2.QtWidgets import QHBoxLayout
    from PySide2.QtWidgets import QSplitter
    from PySide2.QtWidgets import QLabel
    from PySide2.QtWidgets import QTreeView
    from PySide2.QtWidgets import QAction
    from PySide2.QtWidgets import QMenu
    from PySide2.QtWidgets import QAbstractItemView
    from PySide2.QtWidgets import QScrollArea
    from PySide2.QtWidgets import QMessageBox
    from PySide2.QtWidgets import QDialog
    from PySide2.QtWidgets import QFrame
    from PySide2.QtWidgets import QVBoxLayout
    from PySide2.QtWidgets import QGroupBox
    from PySide2.QtWidgets import QRadioButton
    from PySide2.QtWidgets import QCheckBox
    from PySide2.QtWidgets import QLineEdit
    from PySide2.QtWidgets import QDialogButtonBox
    from PySide2.QtWidgets import QPushButton
    from PySide2.QtWidgets import QSpacerItem
    from PySide2.QtWidgets import QSizePolicy
    from PySide2.QtWidgets import QGridLayout
    from PySide2.QtWidgets import QFileDialog
    from PySide2.QtWidgets import QDialog
    from PySide2.QtWidgets import QStyle
    from PySide2.QtWidgets import QInputDialog

    from PySide2 import QtSvg
    from PySide2.QtCore import Qt
    from PySide2.QtCore import QObject
    from PySide2.QtCore import QThread
except ImportError:
    from PyQt5.QtGui import QStandardItemModel
    from PyQt5.QtGui import QStandardItem
    from PyQt5.QtGui import QPixmap
    from PyQt5.QtGui import QIcon
    from PyQt5.QtGui import QPainter
    from PyQt5.QtGui import QPalette
#    from PyQt5.QtMultimedia import QSound

    from PyQt5.QtWidgets import QApplication
    from PyQt5.QtWidgets import QMainWindow
    from PyQt5.QtWidgets import QWidget
    from PyQt5.QtWidgets import QHBoxLayout
    from PyQt5.QtWidgets import QSplitter
    from PyQt5.QtWidgets import QLabel
    from PyQt5.QtWidgets import QTreeView
    from PyQt5.QtWidgets import QAction
    from PyQt5.QtWidgets import QMenu
    from PyQt5.QtWidgets import QAbstractItemView
    from PyQt5.QtWidgets import QScrollArea
    from PyQt5.QtWidgets import QMessageBox
    from PyQt5.QtWidgets import QDialog
    from PyQt5.QtWidgets import QFrame
    from PyQt5.QtWidgets import QVBoxLayout
    from PyQt5.QtWidgets import QGroupBox
    from PyQt5.QtWidgets import QRadioButton
    from PyQt5.QtWidgets import QCheckBox
    from PyQt5.QtWidgets import QLineEdit
    from PyQt5.QtWidgets import QDialogButtonBox
    from PyQt5.QtWidgets import QPushButton
    from PyQt5.QtWidgets import QSpacerItem
    from PyQt5.QtWidgets import QSizePolicy
    from PyQt5.QtWidgets import QGridLayout
    from PyQt5.QtWidgets import QFileDialog
    from PyQt5.QtWidgets import QStyle
    from PyQt5.QtWidgets import QInputDialog

    from PyQt5 import QtSvg
    from PyQt5.QtCore import Qt
    from PyQt5.QtCore import QObject
    from PyQt5.QtCore import QRect
    from PyQt5.QtCore import QSize
    from PyQt5.QtCore import QTimer
    from PyQt5.QtCore import QThread

import enum

class Direction(enum.Enum):
    TOP = 0
    BOTTOM = 1
    RIGHT = 2
    LEFT = 3


class CollideBoundary(Exception):
    pass


class CollideSelf(Exception):
    pass


class PoisonException(Exception):
    pass


class MetaObject():

    def __init__(self, x, y, c):
        self.x = x
        self.y = y
        self.c = c

class Apple(MetaObject):
    def __init__(self, x, y, c = pixmaps.GENERIC_APPLE):
        super().__init__(x, y, c)

    def __str__(self):
        return "A, x: %(x)d, y: %(y)d" % {'x': self.x, 'y': self.y}


class Box(MetaObject):
    def __init__(self, x, y, c = pixmaps.GENERIC_BOX):
        super().__init__(x, y, c)


class Poison(MetaObject):
    def __init__(self, x, y, c = pixmaps.GENERIC_POISON):
        super().__init__(x, y, c)


class Cup(MetaObject):
    def __init__(self, x, y, c = pixmaps.PUCHAR):
        super().__init__(x, y, c)


class Tree(MetaObject):
    def __init__(self, x, y, c = pixmaps.DRZEWKO):
        super().__init__(x, y, c)


class Fox(MetaObject):
    def __init__(self, x, y, c = pixmaps.GENERIC_FOX):
        super().__init__(x, y, c)
        self.counter = 0
        self.direction = Direction.RIGHT

    def move(self):
        if self.direction == Direction.LEFT:
            if self.x == 0:
                self.direction == Direction.RIGHT
                self.x += 1
            else:
                self.x -= 1
        elif self.direction == Direction.RIGHT:
            if self.x == SCREEN_WIDTH-1:
                # Jednak w lewo:
                self.direction == Direction.LEFT
                self.x -= 1
            else:
                self.x += 1
        elif self.direction == Direction.TOP:
            if self.y == 0:
                # Jednak w dół:
                self.direction == Direction.BOTTOM
                self.y += 1
            else:
                self.y -= 1
        elif self.direction == Direction.BOTTOM:
            if self.y == SCREEN_HEIGHT-1:
                # Jednak w górę"
                self.direction == Direction.TOP
                self.y -= 1
            else:
                self.y += 1

    def random_move(self):
        self.counter += 1
        self.counter %= 7
        if self.counter == 0:
            self.direction = random.choice([Direction.LEFT, Direction.RIGHT, Direction.TOP, Direction.BOTTOM])
        self.move()


class Block(MetaObject):
    def __init__(self, x, y, parent, c = pixmaps.GENERIC_BLOCK):
        super().__init__(x, y, c)
        self.parent = parent

    def __eq__(self, other):
        if self.x == other.x and self.y == other.y:
            return True
        return False

    def follow(self, c):
        self.x = c.x
        self.y = c.y

    def left(self):
        if self.x == 0:
            self.x = SCREEN_WIDTH-1
        else:
            self.x -= 1

    def right(self):
        if self.x >= SCREEN_WIDTH-1:
            self.x = 0
        else:
            self.x += 1

    def top(self):
        if self.y == 0:
            self.y = SCREEN_HEIGHT-1
        else:
            self.y -= 1

    def bottom(self):
        if self.y >= SCREEN_HEIGHT-1:
            self.y = 0
        else:
            self.y += 1


class Snake():
    def __init__(self, game, direction, blocks):
        self.blocks = []
        game.snake = self
        self.direction = direction
        if blocks:
            for block in blocks:
                x, y = block
                b = Block(x, y, self)
                self.blocks.append(b)
        self.head = self.blocks[0]
        self.__update_head()
        self.grow = False

    def auto_rotate(self):
        if self.head.x == 0 and self.head.y == 0:
            if self.direction == Direction.LEFT:
                self.direction = Direction.BOTTOM
            elif self.direction == Direction.TOP:
                self.direction == Direction.RIGHT
        elif self.head.x == 0 and self.head.y == SCREEN_HEIGHT-1:
            if self.direction == Direction.BOTTOM:
                self.direction == Direction.RIGHT
            elif self.direction == Direction.LEFT:
                self.direction == Direction.TOP
        elif self.head.x == SCREEN_WIDTH-1 and self.head.y == 0:
            if self.direction == Direction.RIGHT:
                self.direction == Direction.BOTTOM
            elif self.direction == Direction.TOP:
                self.direction == Direction.LEFT
        elif self.head.x == SCREEN_WIDTH-1 and self.head.y == SCREEN_HEIGHT-1:
            if self.direction == Direction.RIGHT:
                self.direction == Direction.TOP
            elif self.direction == Directon.BOTTOM:
                self.direction == Direction.LEFT
        
    def __update_head(self):
        if self.direction == Direction.RIGHT:
            self.head.c = pixmaps.SNAKE_RIGHT
        elif self.direction == Direction.LEFT:
            self.head.c = pixmaps.SNAKE_LEFT
        elif self.direction == Direction.TOP:
            self.head.c = pixmaps.SNAKE_TOP
        elif self.direction == Direction.BOTTOM:
            self.head.c = pixmaps.SNAKE_BOTTOM

    def move(self):
        if self.direction == Direction.RIGHT:
            self.right()
        elif self.direction == Direction.LEFT:
            self.left()
        elif self.direction == Direction.TOP:
            self.top()
        elif self.direction == Direction.BOTTOM:
            self.bottom()

    def __follow_all(self):
        t = self.blocks[-1::-1]
        last = self.blocks[-1]
        bl = Block(last.x, last.y, last.c)
        for i in range(len(t)):
            try:
                b = t[i]
                c = t[i+1]
                b.follow(c)
            except IndexError:
                pass
        if self.grow:
            self.blocks.append(bl)
            self.grow = False

    def collide(self):
        for b in self.blocks[1:]:
            if self.head == b:
                raise CollideSelf()

    def left(self):
        if self.direction != Direction.RIGHT:
            self.__follow_all()
            self.head.left()
            self.direction = Direction.LEFT
            self.__update_head()
            self.collide()

    def right(self):
        if self.direction != Direction.LEFT:
            self.__follow_all()
            self.head.right()
            self.direction = Direction.RIGHT
            self.__update_head()
            self.collide()

    def top(self):
        if self.direction != Direction.BOTTOM:
            self.__follow_all()
            self.head.top()
            self.direction = Direction.TOP
            self.__update_head()
            self.collide()

    def bottom(self):
        if self.direction != Direction.TOP:
            self.__follow_all()
            self.head.bottom()
            self.direction = Direction.BOTTOM
            self.__update_head()
            self.collide()


class Screen(QLabel):

    def __init__(self, parent, width, height):
        super().__init__(parent)
        self.width = width
        self.height = height
        self.setGeometry(QRect(
            0,
            0,
            width * BLOCK_WIDTH,
            height * BLOCK_HEIGHT))
        self.game = None

    def sizeHint(self):
        return QSize(
            self.width * BLOCK_WIDTH,
            self.height * BLOCK_HEIGHT)

    def paint(self):
        c = QPixmap(pixmaps.GENERIC_GRASS)
        pi = QPixmap(
            SCREEN_WIDTH * BLOCK_WIDTH,
            SCREEN_HEIGHT * BLOCK_HEIGHT)
        pa = QPainter()
        pa.begin(pi)
        pa.drawTiledPixmap(
            0,
            0,
            self.width * BLOCK_WIDTH,
            self.height * BLOCK_HEIGHT,
            c)
        if self.game:
            for poison in self.game.poisons:
                qp = QPixmap(poison.c)
                pa.drawPixmap(
                    poison.x * BLOCK_WIDTH,
                    poison.y * BLOCK_HEIGHT,
                    BLOCK_WIDTH,
                    BLOCK_HEIGHT,
                    qp)
            for apple in self.game.apples:
                qp = QPixmap(apple.c)
                pa.drawPixmap(
                    apple.x * BLOCK_WIDTH,
                    apple.y * BLOCK_HEIGHT,
                    BLOCK_WIDTH,
                    BLOCK_HEIGHT,
                    qp)
            for box in self.game.boxes:
                qp = QPixmap(box.c)
                pa.drawPixmap(
                    box.x * BLOCK_WIDTH,
                    box.y * BLOCK_HEIGHT,
                    BLOCK_WIDTH,
                    BLOCK_HEIGHT,
                    qp)
            for cup in self.game.cups:
                qp = QPixmap(cup.c)
                pa.drawPixmap(
                    cup.x * BLOCK_WIDTH,
                    cup.y * BLOCK_HEIGHT,
                    BLOCK_WIDTH,
                    BLOCK_HEIGHT,
                    qp)
            for tree in self.game.trees:
                qp = QPixmap(tree.c)
                pa.drawPixmap(
                    tree.x * BLOCK_WIDTH,
                    tree.y * BLOCK_HEIGHT,
                    BLOCK_WIDTH,
                    BLOCK_HEIGHT,
                    qp)
            for fox in self.game.foxes:
                qp = QPixmap(fox.c)
                pa.drawPixmap(
                    fox.x * BLOCK_WIDTH,
                    fox.y * BLOCK_HEIGHT,
                    BLOCK_WIDTH,
                    BLOCK_HEIGHT,
                    qp)
            for block in self.game.snake.blocks:
                qp = QPixmap(block.c)
                pa.drawPixmap(
                    block.x * BLOCK_WIDTH,
                    block.y * BLOCK_HEIGHT,
                    BLOCK_WIDTH,
                    BLOCK_HEIGHT,
                    qp)

        pa.end()
        self.setPixmap(pi)

class Game():

    def __init__(self, screen, parent):
        self.parent = parent
        screen.game = self
        self.init()

    def init(self):
        self.apples = []
        self.poisons = []
        self.boxes = []
        self.cups = []
        self.trees = []
        self.foxes = []
        self.score = 0

        random.seed()
        for i in range(0, MAX_APPLES):
            self.create_apple()
        for i in range(0, MAX_POISONS):
            self.create_poison()
        self.update_status()
        for i in range(0, MAX_BOXES):
            self.create_box()
        for i in range(0, MAX_CUPS):
            self.create_cup()
        for i in range(0, MAX_TREES):
            self.create_tree()
        for i in range(0, MAX_FOXES):
            self.create_fox()

        self.parent.snake = Snake(
            self,
            Direction.RIGHT,
            [(7, 5), (6, 5), (5, 5), (4, 5), (3, 5), (3, 4)])
    def start(self):
        self.init()
        self.parent.timer.start(200)

    def stop(self):
        self.parent.timer.stop()

    def check_object(self, x, y):
        if len(self.apples) == 0 and len(self.boxes) == 0 and len(self.poisons) == 0:
            return True
        for apple in self.apples:
            if apple.x == x and apple.y == y:
                return False
        for poison in self.poisons:
            if poison.x == x and poison.y == y:
                return False
        for box in self.boxes:
            if box.x == x and box.y == y:
                return False
        for cup in self.cups:
            if cup.x == x and cup.y == y:
                return False
        for tree in self.trees:
            if tree.x == x and tree.y == y:
                 return False
        for fox in self.foxes:
            if fox.x == x and fox.y == y:
                return False
        return True

    def create_apple(self):
        ax = random.randint(0, SCREEN_WIDTH-1)
        ay = random.randint(0, SCREEN_HEIGHT-1)
        while not self.check_object(ax, ay):
            ax = random.randint(0, SCREEN_WIDTH-1)
            ay = random.randint(0, SCREEN_HEIGHT-1)
        apple = Apple(ax, ay)
        self.apples.append(apple)
        return apple

    def create_fox(self):
        ax = random.randint(0, SCREEN_WIDTH-1)
        ay = random.randint(0, SCREEN_HEIGHT-1)
        while not self.check_object(ax, ay):
            ax = random.randint(0, SCREEN_WIDTH-1)
            ay = random.randint(0, SCREEN_HEIGHT-1)
        fox = Fox(ax, ay)
        self.foxes.append(fox)
        return fox

    def create_box(self):
        ax = random.randrange(0, SCREEN_WIDTH-1)
        ay = random.randrange(0, SCREEN_HEIGHT-1)
        while not self.check_object(ax, ay):
            ax = random.randrange(0, SCREEN_WIDTH-1)
            ay = random.randrange(0, SCREEN_HEIGHT-1)
        box = Box(ax, ay)
        self.boxes.append(box)
        return box

    def create_cup(self):
        ax = random.randrange(0, SCREEN_WIDTH-1)
        ay = random.randrange(0, SCREEN_HEIGHT-1)
        while not self.check_object(ax, ay):
            ax = random.randrange(0, SCREEN_WIDTH-1)
            ay = random.randrange(0, SCREEN_HEIGHT-1)
        cup = Cup(ax, ay)
        self.cups.append(cup)
        return cup

    def create_tree(self):
        ax = random.randrange(0, SCREEN_WIDTH-1)
        ay = random.randrange(0, SCREEN_HEIGHT-1)
        while not self.check_object(ax, ay):
            ax = random.randrange(0, SCREEN_WIDTH-1)
            ay = random.randrange(0, SCREEN_HEIGHT-1)
        tree = Tree(ax, ay)
        self.trees.append(tree)
        return tree

    def create_poison(self):
        ax = random.randrange(0, SCREEN_WIDTH-1)
        ay = random.randrange(0, SCREEN_HEIGHT-1)
        while not self.check_object(ax, ay):
            ax = random.randrange(0, SCREEN_WIDTH-1)
            ay = random.randrange(0, SCREEN_HEIGHT-1)
        poison = Poison(ax, ay)
        self.poisons.append(poison)
        return poison

    def update_status(self, msg = ""):
        status = 'MathSnake! | Twój wynik: %(p)d' % {'p': self.score}
        if msg:
            status += ' | ' + msg
        self.parent.setWindowTitle(status)

    def rotate_left(self, snake):
        if snake.direction == Direction.LEFT:
            snake.direction = Direction.BOTTOM
        elif snake.direction == Direction.BOTTOM:
            snake.direction = Direction.RIGHT
        elif snake.direction == Direction.RIGHT:
            snake.direction = Direction.TOP
        elif snake.direction == Direction.TOP:
            snake.direction = Direction.LEFT

    def rotate_right(self, snake):
        if snake.direction == Direction.LEFT:
            snake.direction == Direction.TOP
        elif snake.direction == Direction.TOP:
            snake.direction == Direction.RIGHT
        elif snake.direction == Direction.RIGHT:
            snake.direction == Direction.BOTTOM
        elif snake.direction == Direction.BOTTOM:
            snake.direction == Direction.LEFT

    def move_foxes(self):
        for fox in self.foxes:
            fox.random_move()

    def check_snake(self, snake):
        for apple in self.apples:
            if apple.x == snake.head.x and apple.y == snake.head.y:
                self.apples.remove(apple)
                if self.dodawanie():
                    self.score += 1
                    self.update_status()                    
                self.snake.grow = True
                return
        for box in self.boxes:
            if box.x == snake.head.x and box.y == snake.head.y:
                self.boxes.remove(box)
                self.rotate_left(snake)
                self.play('3.wav')
                if self.odejmowanie():
                    self.score += 1
                    self.update_status()
                self.snake.grow = True
                return
        for poison in self.poisons:
            if poison.x == snake.head.x and poison.y == snake.head.y:
                if self.mnozenie():
                    self.score += 3
                    self.update_status()
                self.poisons.remove(poison)
                self.rotate_right(snake)
                self.snake_grow = True
                return
        for cup in self.cups:
            if cup.x == snake.head.x and cup.y == snake.head.y:
                if self.dzielenie():
                    self.score += 5
                    self.update_status()
                self.cups.remove(cup)
                self.rotate_left(snake)
                self.snake_grow = True
        for tree in self.trees:
            if tree.x == snake.head.x and tree.y == snake.head.y:
                self.trees.remove(tree)
                self.snake_grow = True

        if len(self.apples) + len(self.boxes) + len(self.poisons) + len(self.cups) == 0:
            self.parent.info("Koniec! Twój wynik to: %(w)d" % {'w': self.score})
            if self.parent.pytanie("Czy chcesz zagrać jeszcze raz?"):
                self.init()
                self.start()
            else:
                sys.exit(0)

    def mnozenie(self):
        self.parent.timer.stop()
        first = random.randint(1, 6)
        second = random.randint(1, 6)
        passed = False
        result, status = QInputDialog.getText(self.parent, "Mnożenie", "Podaj wynik mnożenia:\n %(f)d razy %(s)d =" % {'f': first, 's': second})
        if status:
            try:
                result = int(result.strip())
                if result == first * second:
                    passed = True
                else:
                    m = "Poprawna wartość to %(f)d" % {'f': first*second}
                    self.parent.error(m)
            except ValueError:
                m = "Poprawna wartość to %(f)d" % {'f': first*second}
                self.parent.error(m)
        self.parent.timer.start(200)
        return passed

    def dzielenie(self):
        self.parent.timer.stop()
        first = random.randint(1, 6)
        second = random.randint(1, 6)
        second = first * second
        passed = False
        result, status = QInputDialog.getText(self.parent, "Dzielenie", "Podaj wynik dzielenia:\n %(f)d : %(s)d =" % {'f': second, 's': first})
        if status:
            try:
                result = int(result.strip())
                if result == second / first:
                    passed = True
                else:
                    m = "Poprawna wartość to %(f)d" % {'f': second / first}
                    self.parent.error(m)
            except ValueError:
                m = "Poprawna wartość to %(f)d" % {'f': second / first}
                self.parent.error(m)
        self.parent.timer.start(200)
        return passed

    def dodawanie(self):
        self.parent.timer.stop()
        first = random.randint(1, 15)
        second = random.randint(1, 15)
        passed = False
        result, status = QInputDialog.getText(self.parent, "Dodawanie", "Podaj wynik dodawania:\n %(f)d + %(s)d =" % {'f': first, 's': second})
        if status:
            try:
                result = int(result.strip())
                if result == first + second:
                    passed = True
                else:
                    m = "Poprawna wartość to %(f)d" % {'f': first + second}
                    self.parent.error(m)
            except ValueError:
                m = "Poprawna wartość to %(f)d" % {'f': first + second}
                self.parent.error(m)
        self.parent.timer.start(200)
        return passed

    def odejmowanie(self):
        self.parent.timer.stop()
        first = random.randint(1, 20)
        second = random.randint(1, 20)
        passed = False
        if second > first:
            first, second = second, first
        result, status = QInputDialog.getText(self.parent, "Odejmowanie", "Podaj wynik odejmowania:\n%(f)d - %(s)d =" % {'f': first, 's': second})
        if status:
            try:
                result = int(result.strip())
                if result == first - second:
                    passed = True
                else:
                    m = "Poprawna wartość to %(f)d" % {'f': first - second}
                    self.parent.error(m)
            except ValueError:
                m = "Poprawna wartość to %(f)d" % {'f': first - second}
                self.parent.error(m)
        self.parent.timer.start(200)
        return passed

    def play(self, file):
#        QSound.play(file)
        pass


class HWindow(QMainWindow):

    def __init__(self):
        super().__init__()
        self.create_ui()
        self.move(80, 80)

    def create_ui(self):
        self.screen = Screen(self, SCREEN_WIDTH, SCREEN_HEIGHT)
        self.game = Game(self.screen, self)
        self.setCentralWidget(self.screen)
        self.screen.paint()
        self.timer = QTimer()
        self.timer.timeout.connect(self.tick)
        self.info("Naciśnij Enter, gdy będziesz gotowy...")
        self.timer.start(200)

    def error(self, msg = ""):
        mb = QMessageBox()
        mb.setIcon(QMessageBox.Warning)
        mb.setText(msg)
        mb.setWindowTitle("Uch!")
        mb.setStandardButtons(QMessageBox.Ok)
        mb.exec_()

    def info(self, msg = ""):
        mb = QMessageBox(self)
        mb.setIcon(QMessageBox.Information)
        mb.setText(msg)
        mb.setWindowTitle("Informacja")
        mb.setStandardButtons(QMessageBox.Ok)
        mb.exec_()

    def pytanie(self, msg = ""):
        mb = QMessageBox()
        response = mb.question(self, "Pytanie", msg, mb.Yes | mb.No)
        return True if response == mb.Yes else False

    def tick(self):
        try:
            self.snake.move()
            self.game.move_foxes()
            self.screen.paint()
            self.game.check_snake(self.snake)
        except CollideSelf:
            self.timer.stop()
            self.error("Zjadłeś samego siebie! To koniec! I kropka!")
            if self.pytanie("Czy chcesz zagrać jeszcze raz?"):
                self.game.init()
                self.game.start()
            else:
                sys.exit(0)

    def keyPressEvent(self, event):
        try:
            key = event.key()
            if key == Qt.Key_Left:
                self.snake.left()
                self.screen.paint()
                self.game.check_snake(self.snake)
            elif key == Qt.Key_Right:
                self.snake.right()
                self.screen.paint()
                self.game.check_snake(self.snake)
            elif key == Qt.Key_Up:
                self.snake.top()
                self.screen.paint()
                self.game.check_snake(self.snake)
            elif key == Qt.Key_Down:
                self.snake.bottom()
                self.screen.paint()
                self.game.check_snake(self.snake)
        except CollideSelf:
            self.timer.stop()
            self.error("Zjadłeś samego siebie! To koniec! I kropka!")
            if self.pytanie("Czy chcesz zagrać jeszcze raz?"):
                self.game.init()
                self.game.start()
            else:
                sys.exit(0)

if __name__ == "__main__":
    random.seed()
    app = QApplication(sys.argv)
    window = HWindow()
    window.show()
    sys.exit(app.exec_())
